<?php
require_once 'remote_router_1DB_Function.php';
$db = new Other_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

$mac = $data["mac"];
$name = $data["name"];
if (isset($mac) && isset($name)) {
    if (!empty($mac) && !empty($name)) {
        if ($db->isOuiReady($mac)) {
            $response["status"] = "success";
            $response["message"] = "Oui Ready";
            echo json_encode($response);
        } else {
            $oui = $db->setOuiData($mac, $name);
            if ($oui) {
                $response["status"] = "success";
                $response["message"] = "Set Success";
                echo json_encode($response);
            } else {
                $response["status"] = "failed";
                $response["message"] = "Set Failed";
                echo json_encode($response);
            }
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Empty Parameters";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}
?>