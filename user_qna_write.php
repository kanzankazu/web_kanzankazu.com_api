<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data["email"]) && isset($data["password1"]) && isset($data["tanya1"]) && isset($data["tanya2"]) && isset($data["jawab1"]) && isset($data["jawab2"])) {

    $email = $data['email'];
    $password1 = $data['password1'];
    $passmd = md5($password1);
    $tanya1 = $data['tanya1'];
    $tanya2 = $data['tanya2'];
    $jawab1 = $data['jawab1'];
    $jawab2 = $data['jawab2'];

    $checkEmPass = $db->isEmPassExisted($email, $passmd);
    if ($checkEmPass) {
        if ($db->setQna($email, $tanya1, $tanya2, $jawab1, $jawab2)) {
            $response["status"] = "success";
            $response["message"] = "save success";
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["message"] = "save failed";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = 'User Not Registered';
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "invalid parameter";
    echo json_encode($response);
}
?>