<?php
require_once 'user_1DB_Function.php';
require_once '12_Config_support.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data["email"]) && isset($data['password1']) && isset($data['password2'])) {

    $emailcode = make_unique_id_by_lenght(30);
    $uid = gen_uuid3();

    $email = $data['email'];
    $piecesemail = explode("@", $email);
    $email1 = $piecesemail[0];//bagian depan
    $email2 = $piecesemail[1];//bagian belakang

    $name = $data['name'];
    $password1a = ($data['password1']);
    $password1b = md5($password1a);
    $password2a = ($data['password2']);
    $enctpassword = md5($password2a);
    $phonenmbr = $data['phonenmbr'];
    $usercode = make_unique_id_by_lenght(10);

    if (!$db->isEmailExisted($email)) {
        if (!$db->isEmPassExisted($email, $password1b)) {
            if (isset($phonenmbr) && !$db->isPhoneExisted($phonenmbr)) {
                $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 2;                                           // Enable verbose debug output
                    $mail->isSMTP();                                                // Set mailer to use SMTP
                    $mail->Host = 'mail.kanzankazu.com';                            // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                                         // Enable SMTP authentication
                    $mail->Username = 'norelpy@kanzankazu.com';                     // SMTP username
                    $mail->Password = 'kanzan12122012';                             // SMTP password
                    $mail->SMTPSecure = 'tls';                                      // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587;                                              // TCP port to connect to

                    //Recipients
                    $mail->setFrom('norelpy@kanzankazu.com', 'kanzankazu.com');
                    $mail->addAddress($email, $name);                               // Add a recipient
                    //$mail->addAddress('kazukanzan46@gmail.com');                  // Name is optional
                    //$mail->addReplyTo('kanzan46@gmail.com', 'Information');
                    $mail->addCC('kanzan46@gmail.com');
                    //$mail->addBCC('bcc@example.com');

                    //Attachments
                    //$mail->addAttachment('/var/tmp/file.tar.gz');                 // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');            // Optional name

                    //Content
                    $mail->IsHTML(true);
                    $mail->Subject = "Konfirmasi User KanzanKazuDev Ticket #" . make_unique_id_by_lenght(10);
                    $mail->Body =
                    $body =
                        "<body style='margin: 10px;'>
                        <div style='width:100%; font-family: Helvetica, sans-serif; font-size: 13px; padding:10px; line-height:150%; border:#eaeaea solid 10px;'>
                            <br>
                            <h2><strong>Terima Kasih Telah Mendaftar di <a href='http://kanzankazu.com'>KanzanKazu.COM</a></strong></h2><br>
                            <hr>
                            <b>Nama  	: </b>" . $name . "<br>
                            <b>email 	: </b>" . $email . "<br>
                            <b>No.Telp 	: </b>" . $phonenmbr . "<br>
                            <b>Password : </b>" . string_stars_middle($password1a) . "<br>
                            <a style='background-color: #0000ff; padding:2px; font-size: small;color: #ffffff;border: solid 2px;border-radius: 5px;border-color: #00007d' href='http://" . $_SERVER['SERVER_NAME'] . "/androidAPI/confirm/" . $emailcode . "'><b>CONFIRM</b></a>
                            <br>
                        </div>
                    </body>";
                    //$mail->AltBody = "Ini adalah percobaan mengirim email dengan PHP";

                    if (!$mail->Send()) {
                        $response["status"] = "failed";
                        $response["message"] = "Send Email Failed : " . $mail->ErrorInfo;
                        echo json_encode($response);
                    } else {
                        $simpan = $db->setUser($email, $name, $email1, $phonenmbr, $enctpassword, $emailcode, $uid, $usercode);
                        if ($simpan) {
                            $response["status"] = "success";
                            $response["message"] = "Save Success";
                            echo json_encode($response);
                        } else {
                            $response["status"] = "failed";
                            $response["message"] = "Save Failed";
                            echo json_encode($response);
                        }
                    }
                } catch (Exception $e) {
                    $response["status"] = "failed";
                    $response["message"] = "Send Email Failed : " . $mail->ErrorInfo;
                    echo json_encode($response);
                }
            } else if (!isset($phonenmbr)) {
                $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 2;                                           // Enable verbose debug output
                    $mail->isSMTP();                                                // Set mailer to use SMTP
                    $mail->Host = 'mail.kanzankazu.com';                            // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                                         // Enable SMTP authentication
                    $mail->Username = 'norelpy@kanzankazu.com';                     // SMTP username
                    $mail->Password = 'kanzan12122012';                             // SMTP password
                    $mail->SMTPSecure = 'tls';                                      // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587;                                              // TCP port to connect to

                    //Recipients
                    $mail->setFrom('norelpy@kanzankazu.com', 'kanzankazu.com');
                    $mail->addAddress($email, $name);                               // Add a recipient
                    //$mail->addAddress('kazukanzan46@gmail.com');                  // Name is optional
                    //$mail->addReplyTo('kanzan46@gmail.com', 'Information');
                    $mail->addCC('kanzan46@gmail.com');
                    //$mail->addBCC('bcc@example.com');

                    //Attachments
                    //$mail->addAttachment('/var/tmp/file.tar.gz');                 // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');            // Optional name

                    //Content
                    $mail->IsHTML(true);
                    $mail->Subject = "Konfirmasi User KanzanKazuDev Ticket #" . make_unique_id_by_lenght(10);
                    $mail->Body =
                    $body =
                        "<body style='margin: 10px;'>
                        <div style='width:100%; font-family: Helvetica, sans-serif; font-size: 13px; padding:10px; line-height:150%; border:#eaeaea solid 10px;'>
                            <br>
                            <h2><strong>Terima Kasih Telah Mendaftar di <a href='http://kanzankazu.com'>KanzanKazu.COM</a></strong></h2><br>
                            <hr>
                            <b>Nama  	: </b>" . $name . "<br>
                            <b>email 	: </b>" . $email . "<br>
                            <b>No.Telp 	: </b>" . $phonenmbr . "<br>
                            <b>Password : </b>" . string_stars_middle($password1a) . "<br>
                            <a style='background-color: #0000ff; padding:2px; font-size: small;color: #ffffff;border: solid 2px;border-radius: 5px;border-color: #00007d' href='http://" . $_SERVER['SERVER_NAME'] . "/androidAPI/confirm/" . $emailcode . "'><b>CONFIRM</b></a>
                            <br>
                        </div>
                    </body>";
                    //$mail->AltBody = "Ini adalah percobaan mengirim email dengan PHP";

                    if (!$mail->Send()) {
                        $response["status"] = "failed";
                        $response["message"] = "Send Email Failed : " . $mail->ErrorInfo;
                        echo json_encode($response);
                    } else {
                        $simpan = $db->setUser($email, $name, $email1, "", $enctpassword, $emailcode, $uid, $usercode);
                        if ($simpan) {
                            $response["status"] = "success";
                            $response["message"] = "Save Success";
                            echo json_encode($response);
                        } else {
                            $response["status"] = "failed";
                            $response["message"] = "Save Failed";
                            echo json_encode($response);
                        }
                    }
                } catch (Exception $e) {
                    $response["status"] = "failed";
                    $response["message"] = "Send Email Failed : " . $mail->ErrorInfo;
                    echo json_encode($response);
                }
            } else {
                $response["status"] = "failed";
                $response["message"] = "Phone number Exist";
                echo json_encode($response);
            }
        } else {
            $response["status"] = "failed";
            $response["message"] = "User Exist";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Users Exist";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "invalid parameter";
    echo json_encode($response);
}

?>