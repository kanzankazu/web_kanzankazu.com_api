<?php

function check_internet_connection()
{
    $connected = @fsockopen("www.google.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function check_avaliable_data_table($TableName, $RowName, $DataRowName)
{
    $sql = mysqli_query($this->conn, "select * from $TableName WHERE $RowName='$DataRowName'");
    $hasil = mysqli_num_rows($sql);
    if ($hasil == 1) {
        return true;
    } else {
        return false;
    }
}

function get_rownum_table($TableName, $RowName)
{
    $sql = mysqli_query(mysqli_connect(), "select $RowName $TableName by $RowName desc limit 1");
    $hasil = mysqli_fetch_array($sql);
    $number = 1 + $hasil[$RowName];
    return $number;
}

/**
 * membuat unique id sesuai panjang
 * @param $int_PanajangHuruf
 * @return string
 */
function make_unique_id_by_lenght($int_PanajangHuruf)
{
    //tentukan karakter random;
    $karakter = 'ABCEFGHIJKLMNOPQRSTUVWXYZabcdefghtijklmnopqrstuvwxyz1234567890';
    //kembalian (return)
    //buat variable kosong
    $string = '';
    //lakukan proseslooping
    for ($i = 0; $i < $int_PanajangHuruf; $i++) {
        //lakukan random data dengan nilai awal 0
        //dan nilai akhir sebanyak nikai $karakter
        $pos = rand(0, strlen($karakter) - 1);
        //penggabungan dengan nilai kanan dan kiri
        $string .= $karakter{$pos};
    }
    //kembalikan nilai  dari fuction  ke echo
    return $string;
}

function make_unique_id_by_time_IP()
{
    $time_stamp = date("Ymdhis");
    $ip_add = $_SERVER['REMOTE_ADDR'];
    $orderid = "$time_stamp-$ip_add";
    $unique = str_replace(".", "", "$orderid");
    return $unique;
}

function make_unique_XX_XXX_XX()
{
    $create = strtoupper(md5(uniqid(rand(), true)));
    $style =
        substr($create, 0, 8) . '-' .
        substr($create, 8, 4) . '-' .
        substr($create, 12, 4) . '-' .
        substr($create, 16, 4) . '-' .
        substr($create, 20);
    return $style;
}

function string_to_dash($str)
{
    $str1 = str_replace('&', '', strtolower($str));
    $str2 = str_replace('(', '', strtolower($str1));
    $str3 = str_replace(')', '', strtolower($str2));
    $str4 = str_replace(' ', '-', strtolower($str3));
    $str5 = str_replace('.', '-', strtolower($str4));
    $str6 = str_replace(',', '-', strtolower($str5));
    $str7 = str_replace('/', '-', strtolower($str6));
    $str8 = str_replace('--', '-', strtolower($str7));
    return $str8;
}

function string_to_underscore($str)
{
    $str1 = str_replace('&', ' ', strtolower($str));
    $str2 = str_replace('(', ' ', strtolower($str1));
    $str3 = str_replace(')', ' ', strtolower($str2));
    $str4 = str_replace(' ', '_', strtolower($str3));
    $str5 = str_replace('.', '_', strtolower($str4));
    $str6 = str_replace(',', '_', strtolower($str5));
    $str7 = str_replace('/', '_', strtolower($str6));
    $str8 = str_replace('--', '_', strtolower($str7));
    $str9 = str_replace('__', '_', strtolower($str8));
    return $str9;
}

function string_to_plus($str)
{
    $str1 = str_replace('&', ' ', strtolower($str));
    $str2 = str_replace('(', ' ', strtolower($str1));
    $str3 = str_replace(')', ' ', strtolower($str2));
    $str4 = str_replace(' ', '+', strtolower($str3));
    $str5 = str_replace('.', '+', strtolower($str4));
    $str6 = str_replace(',', '+', strtolower($str5));
    $str7 = str_replace('/', '+', strtolower($str6));
    $str8 = str_replace('--', '+', strtolower($str7));
    $str9 = str_replace('__', '+', strtolower($str8));
    $str10 = str_replace("'", '+', strtolower($str8));
    return $str10;
}

function string_to_nospacing($str)
{
    $str1 = str_replace('&', '', strtolower($str));
    $str2 = str_replace('(', '', strtolower($str1));
    $str3 = str_replace(')', '', strtolower($str2));
    $str4 = str_replace(' ', '', strtolower($str3));
    $str5 = str_replace('.', '', strtolower($str4));
    $str6 = str_replace(',', '', strtolower($str5));
    $str7 = str_replace('/', '', strtolower($str6));
    $str8 = str_replace('--', '', strtolower($str7));
    $str9 = str_replace('__', '', strtolower($str8));
    return $str9;
}

/**
 * membuat bagian tengah text menjadi bintang
 * @param $str
 * @return mixed
 */
function string_stars_middle($str)
{
    return preg_replace("/(?!^).(?!$)/", "*", $str);
}

/**
 * kirim email
 * @param $tujuanemail
 * @param $namaemail
 * @param $subjectemail
 * @param $isiemail
 * @return string
 */
function send_email($tujuanemail, $namaemail, $subjectemail, $isiemail)
{
    require('library/PHPMailer-master/class.phpmailer.php');
    require("library/PHPMailer-master/class.smtp.php");
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "ssl";
    //$mail->SMTPDebug = 2;
    $mail->Username = "kanzankazu46@gmail.com";
    $mail->Password = "kanzan2409";
    $webmaster_email = "kanzankazu46@gmail.com";
    $mail->From = $webmaster_email;
    $mail->FromName = "kanzankazu";
    $mail->addAddress($tujuanemail, $namaemail);
    $mail->AddReplyTo($webmaster_email, $mail->FromName);
    $mail->WordWrap = 50;
    $mail->IsHTML(true);
    $mail->Subject = "$subjectemail";
    $mail->Body = $isiemail;
    if (!$mail->Send()) {
        return "Gagal mengirim email: " . $mail->ErrorInfo;
    } else {
        return "email send";
    }
}

function directory_remove($path)
{
    // The preg_replace is necessary in order to traverse certain types of folder paths (such as /dir/[[dir2]]/dir3.abc#/)
    // The {,.}* with GLOB_BRACE is necessary to pull all hidden files (have to remove or get "Directory not empty" errors)
    $files = glob(preg_replace('/(\*|\?|\[)/', '[$1]', $path) . '/{,.}*', GLOB_BRACE);
    foreach ($files as $file) {
        if ($file == $path . '/.' || $file == $path . '/..') {
            continue;
        } // skip special dir entries
        is_dir($file) ? directory_remove($file) : unlink($file);
    }
    if (rmdir($path)) {
        return true;
    } else {
        return false;
    }
}

function getdirectory_by_device()
{
    $tablet_browser = 0;
    $mobile_browser = 0;
    if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
        $uri = 'https://';
    } else {
        $uri = 'http://';
    }
    $uri .= $_SERVER['HTTP_HOST'];
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $tablet_browser++;
    }
    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }
    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
        $mobile_browser++;
    }
    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
        'w3c ', 'acs-', 'alav', 'alca', 'amoi', 'audi', 'avan', 'benq', 'bird', 'blac',
        'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno',
        'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-',
        'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-',
        'newt', 'noki', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox',
        'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar',
        'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-',
        'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp',
        'wapr', 'webc', 'winw', 'winw', 'xda ', 'xda-');
    if (in_array($mobile_ua, $mobile_agents)) {
        $mobile_browser++;
    }
    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'opera mini') > 0) {
        $mobile_browser++;
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA']) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $tablet_browser++;
        }
    }
    if ($tablet_browser > 0) {
        // do something for tablet devices
        return 'kanzankazu_mobile';
    } else if ($mobile_browser > 0) {
        // do something for mobile devices
        return 'kanzankazu_mobile';
    } else {
        // do something for everything else
        return 'kanzankazu';
    }
}

function gen_uuid1($data)
{
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function gen_uuid2()
{
    if (function_exists('com_create_guid') === true)
        return trim(com_create_guid(), '{}');

    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function gen_uuid3()
{
    $randomString = openssl_random_pseudo_bytes(16);
    $time_low = bin2hex(substr($randomString, 0, 4));
    $time_mid = bin2hex(substr($randomString, 4, 2));
    $time_hi_and_version = bin2hex(substr($randomString, 6, 2));
    $clock_seq_hi_and_reserved = bin2hex(substr($randomString, 8, 2));
    $node = bin2hex(substr($randomString, 10, 6));

    /**
     * Set the four most significant bits (bits 12 through 15) of the
     * time_hi_and_version field to the 4-bit version number from
     * Section 4.1.3.
     * @see http://tools.ietf.org/html/rfc4122#section-4.1.3
     */
    $time_hi_and_version = hexdec($time_hi_and_version);
    $time_hi_and_version = $time_hi_and_version >> 4;
    $time_hi_and_version = $time_hi_and_version | 0x4000;

    /**
     * Set the two most significant bits (bits 6 and 7) of the
     * clock_seq_hi_and_reserved to zero and one, respectively.
     */
    $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
    $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
    $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;

    return sprintf('%08s-%04s-%04x-%04x-%012s', $time_low, $time_mid, $time_hi_and_version, $clock_seq_hi_and_reserved, $node);
} // guid

function gen_uuid4()
{
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}

function gen_uuid5()
{
    $uuid = array(
        'time_low' => 0,
        'time_mid' => 0,
        'time_hi' => 0,
        'clock_seq_hi' => 0,
        'clock_seq_low' => 0,
        'node' => array()
    );

    $uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
    $uuid['time_mid'] = mt_rand(0, 0xffff);
    $uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
    $uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
    $uuid['clock_seq_low'] = mt_rand(0, 255);

    for ($i = 0; $i < 6; $i++) {
        $uuid['node'][$i] = mt_rand(0, 255);
    }

    $uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
        $uuid['time_low'],
        $uuid['time_mid'],
        $uuid['time_hi'],
        $uuid['clock_seq_hi'],
        $uuid['clock_seq_low'],
        $uuid['node'][0],
        $uuid['node'][1],
        $uuid['node'][2],
        $uuid['node'][3],
        $uuid['node'][4],
        $uuid['node'][5]
    );

    return $uuid;
}

?>