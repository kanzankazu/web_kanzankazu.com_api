<?php
class User_DB_Functions
{
    private $conn;

    // constructor
    function __construct()
    {
        require_once '1DB_Connect.php';
        // koneksi ke database
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }

    function __destruct()
    {

    }

    /**
     * @param $serverKey
     * @param $title
     * @param $message
     * @param $isUpdate boolean
     * @param $isForceUpdate boolean
     * @param $updateInfo string
     * @return mixed
     */
    public function notifUpdateAll($serverKey, $title, $message, $isUpdate, $isForceUpdate, $updateInfo)
    {
        $data = array(
            "update" => $isUpdate,
            "force_update" => $isForceUpdate,
            "update_info" => $updateInfo
        );

        $notification = array
        (
            'body' => "$message",
            'sound' => 'default',
            'title' => "$title",
            //'tickerText' => 'kanzan',
            'is_background' => true,
            'payload' => array('my-data-item' => 'my-data-value'),
            'timestamp' => date('Y-m-d G:i:s')
        );

        //$url = 'https://fcm.googleapis.com/fcm/send';
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $headers = array(
            //'Authorization:key = AIzaSyCFTvM60CHfPINRWDYdYBcU5bBkV2XkoB4', //Web API Key => setting app firebase console (Legacy server key)
            'Authorization:key = ' . $serverKey . '', //Web API Key => setting app firebase console (Legacy server key)
            'Content-Type: application/json'
        );

        $fields = array
        (

//            'registration_ids' => $tokens,//share to individual
            'to' => '/topics/app',//share to topic in this project
            'data' => $data,
            'notification' => $notification,
            'priority' => 'high'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function notifChat($serverKey, $user_token, $title, $message)
    {
        $data = array(
            "message" => $message,
            "group" => true
        );

        $notification = array
        (
            'body' => "$message",
            'sound' => 'default',
            'title' => "$title",
            //'tickerText' => 'kanzan',
            'is_background' => true,
            'payload' => array('my-data-item' => 'my-data-value'),
            'timestamp' => date('Y-m-d G:i:s')
        );

        //$url = 'https://fcm.googleapis.com/fcm/send';
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $headers = array(
            //'Authorization:key = AIzaSyCFTvM60CHfPINRWDYdYBcU5bBkV2XkoB4', //Web API Key => setting app firebase console (Legacy server key)
            'Authorization:key = ' . $serverKey . '', //Web API Key => setting app firebase console (Legacy server key)
            'Content-Type: application/json'
        );

        $fields = array
        (
            'registration_ids' => $user_token,//share to individual
//            'to' => '/topics/app',//share to topic in this project
            'data' => $data,
            'notification' => $notification,
            'priority' => 'high'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function notifGroup($serverKey, $group, $title, $message)
    {
        $data = array(
            "message" => $message,
            "group" => true
        );

        $notification = array
        (
            'body' => "$message",
            'sound' => 'default',
            'title' => "$title",
            //'tickerText' => 'kanzan',
            'is_background' => true,
            'payload' => array('my-data-item' => 'my-data-value'),
            'timestamp' => date('Y-m-d G:i:s')
        );

        //$url = 'https://fcm.googleapis.com/fcm/send';
        $url = 'https://gcm-http.googleapis.com/gcm/send';

        $headers = array(
            //'Authorization:key = AIzaSyCFTvM60CHfPINRWDYdYBcU5bBkV2XkoB4', //Web API Key => setting app firebase console (Legacy server key)
            'Authorization:key = ' . $serverKey . '', //Web API Key => setting app firebase console (Legacy server key)
            'Content-Type: application/json'
        );

        $fields = array
        (
//            'registration_ids' => $token,//share to individual
            'to' => '/topics/' . $group . '',//share to topic in this project
            'data' => $data,
            'notification' => $notification,
            'priority' => 'high'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}