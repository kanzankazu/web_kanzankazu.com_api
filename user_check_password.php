<?php
require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

$data = json_decode(file_get_contents('php://input'), true);

header('Content-Type: application/json');

if (isset($data['email']) && isset($data['password1'])) {

    $email = $data['email'];
    $pass1 = ($data['password1']);
    $passmd1 = md5($pass1);

    if ($db->isEmPassExisted($email, $passmd1)) {
        $response["status"] = "success";
        $response["message"] = "User Registered";
        $users = $db->getUserByEmailPassword($email, $passmd1);
        $response["uid"] = $users["uid"];
        $response["userlevel"] = $users["userlevel"];
        $response["email"] = $users["email"];
        $response["name"] = $users["name"];
        $response["birthdt"] = $users["birthdt"];
        $response["gender"] = $users["gender"];
        $response["phonenmbr"] = $users["phonenmbr"];
        $response["usercode"] = $users["usercode"];
        echo json_encode($response);
    } else {

        $response["status"] = "failed";
        $response["message"] = "User Not Exist";
        echo json_encode($response);
    }
} else {

    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}
?>