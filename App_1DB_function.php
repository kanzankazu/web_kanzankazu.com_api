<?php

class App_DB_function
{
    private $conn;

    function __construct()
    {
        require_once "1DB_Connect.php";

        $db = new DB_Connect();
        $this->conn = $db->connect();

    }

    function __destruct()
    {

    }

    //APP

    /**
     * @param $appPkg
     * @param $appVersion
     * @return bool
     * jika (tidak ada data true)/false
     */
    public function isAppReadyByID($appPkg)
    {
        $stmt = $this->conn->prepare("SELECT * FROM appversion WHERE apppkg = ?");
        $stmt->bind_param("s", $appPkg);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    /**
     * @param $appName
     * @return bool
     */
    public function isAppReadyByName($appName)
    {
        $stmt = $this->conn->prepare("SELECT * FROM appversion WHERE appname=?");
        $stmt->bind_param("s", $appName);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    /**
     * @param $appVersion
     * @return bool
     */
    public function isAppReadyByVersion($appVersion)
    {
        $stmt = $this->conn->prepare("SELECT * FROM appversion WHERE appversion=?");
        $stmt->bind_param("s", $appVersion);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    /**
     * @param $appPkg
     * @param $appVersion
     * @return bool
     */
    public function isAppUpdate($appPkg, $appVersion)
    {
        $stmt = $this->conn->prepare("SELECT * FROM appversion WHERE apppkg =? AND appversion = ?");
        $stmt->bind_param("ss", $appPkg, $appVersion);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return false;
        } else {
            $stmt->close();
            return true;
        }
    }

    /**
     * @param $appPkg
     * @return array
     */
    public function getAppData($appPkg)
    {
        $stmt = $this->conn->prepare("SELECT applink,appversion,appupdatedt FROM appversion WHERE apppkg = ?");
        $stmt->bind_param("s", $appPkg);
        $stmt->execute();
        //$app = $stmt->get_result()->fetch_assoc();
        $app = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $app[0];
    }
}

?>