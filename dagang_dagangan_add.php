<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['uid']) && isset($data['dagnm']) && isset($data['dagtype'])) {
    if (!empty($data['daglat']) && !empty($data['daglng'])) {
        $dagid = $db->getLastNumTable("dagangan", "dagid");
        $barangId = $db->getLastNumTable("daganganbrg", "dagbrngid");
        $uid = $data['uid'];
        $dagangIdCtgy = $data['dagctgyid'];
        $dagangNm = $data['dagnm'];
        $dagangSlogan = $data['dagslogan'];
        $dagangType = $data['dagtype'];
        $dagangStart = $data['dagstart'];
        $dagangEnd = $data['dagend'];
        $dagangAuto = $data['dagauto'];
        $dagangOrder = $data['dagorder'];
        $dagangOnlyOrder = $data["dagorderonly"];
        $dagangDpOrder = $data["dagorderdp"];
        $dagangDpMinOrder = $data["dagorderdpmin"];
        $dagangAgen = $data['dagagen'];
        $dagangSend = $data['dagsend'];
        $dagangPhnNmbr = $data['dagphnmbr'];
        $dagangPhnNmbrVisible = $data['dagphnmbrhide'];
        $dagangLat = $data['daglat'];
        $dagangLng = $data['daglng'];
        $dagangAddr = $data['dagaddr'];
        $barangNm = $data['dagbrngnm'];

        $dagangCode = gen_uuid3();
        $limitDag = $db->getLimitDagangan($uid);
        $countDag = $db->getCountDagangan($uid);

        if ($db->isUserExist($uid)) {
            if ($countDag < $limitDag) {
                $setDagangan = $db->setDagangan(
                    $dagid, $uid,
                    $dagangIdCtgy, $dagangNm,
                    $dagangSlogan, $dagangType,
                    $dagangOrder, $dagangOnlyOrder,
                    $dagangDpOrder, $dagangDpMinOrder,
                    $dagangSend, $dagangAgen,
                    $dagangPhnNmbrVisible, $dagangPhnNmbr,
                    $dagangLat, $dagangLng,
                    $dagangAddr, $dagangAuto,
                    $dagangStart, $dagangEnd,
                    $dagangCode);

                if ($setDagangan) {
                    if ($db->setBarangArray($barangId, $dagid, $barangNm)) {
                        $response['status'] = "success";
                        $response['message'] = "Success Set Barang";
                        echo json_encode($response);
                    } else {
                        $response['status'] = "failed";
                        $response['message'] = "Failed Set Barang";
                        echo json_encode($response);
                    }
                } else {
                    $response['status'] = "failed";
                    $response['message'] = "Failed Set Dagangan";
                    echo json_encode($response);
                }
            } else {
                $response["status"] = "failed";
                $response["message"] = "Dagangan has Reach Limit, Please Check Your Limit";
                echo json_encode($response);
            }
        } else {
            $response['status'] = "failed";
            $response['message'] = "User Not Exist";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Location Dagangan Not Detect";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>