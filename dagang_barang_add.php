<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['dagid']) && isset($data['dagbrngnm'])) {

    $dagid = $data['dagid'];
    $dagbrngnm = $data['dagbrngnm'];

    if ($db->isdagIdExisted($dagid)) {
        if ($db->checkBarang($dagid, $dagbrngnm)) {
            if ($db->setBarang($dagid, $dagbrngnm)) {
                // app ditemukan update
                $response['status'] = "success";
                $response['message'] = "Save Success";
                echo json_encode($response);
            } else {
                // app tidak ditemukan update
                $response['status'] = "failed";
                $response['message'] = "Save Failed";
                echo json_encode($response);
            }
        } else {
            $response['status'] = "failed";
            $response['message'] = "Barang Already";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Dagangan Not Already";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>