<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

$category = $db->getAllCategory();
if ($category) {
    $response['status'] = "success";
    $response['message'] = "Success get Category";
    $response["category"] = $category;
    echo json_encode($response);
} else {
    $response['status'] = "failed";
    $response['message'] = "Failed get Category";
    echo json_encode($response);
}
?>