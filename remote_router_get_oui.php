<?php
require_once 'remote_router_1DB_Function.php';
$db = new Other_DB_Functions();

header('Content-Type: application/json');

$oui = $db->getOuiData();

if ($oui) {
    $response["status"] = "success";
    $response["message"] = "Oui Ready";
    $response["data"] = $oui;

    echo json_encode($response);
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    $response["data"] = "";
    echo json_encode($response);
}

?>