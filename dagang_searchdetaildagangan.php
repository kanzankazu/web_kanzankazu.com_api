<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if(isset($data['dagid'])){
    $dagid = $data['dagid'];
    $dataResult = $db->searchByDagId($dagid);
    if ($dataResult) {
        $response['status'] = "success";
        $response['message'] = "Success Get Data";
        $response["data"] = $dataResult;
        echo json_encode($response);
    } else {
        $response['status'] = "failed";
        $response['message'] = "Failed Get Data";
        echo json_encode($response);
    }
}else{
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameter";
    echo json_encode($response);
}
?>