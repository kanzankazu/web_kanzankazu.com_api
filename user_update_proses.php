<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

$uid = $data['uid'];
$name = $data['name'];
$email = $data['email'];
$birthdt = $data['birthdt'];
$phonenmbr = $data['phonenmbr'];
$gender = $data['gender'];
$password = $data['password'];
$passmd1 = md5($password);

if (isset($name) && isset($uid) && isset($birthdt) && isset($phonenmbr) && isset($gender)) {
    if ($db->isUidPassExisted($uid, $passmd1)) {
        if ($db->updateUser($uid, $name, $birthdt, $phonenmbr, $gender)) {
            $response["status"] = "success";
            $response["message"] = "Update Success";
            $users = $db->getUserByUid($uid);
            $response["uid"] = $users["uid"];
            $response["userlevel"] = $users["userlevel"];
            $response["email"] = $users["email"];
            $response["name"] = $users["name"];
            $response["birthdt"] = $users["birthdt"];
            $response["gender"] = $users["gender"];
            $response["phonenmbr"] = $users["phonenmbr"];
            $response["usercode"] = $users["usercode"];
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["message"] = "Update Failed";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Wrong Password";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameter";
    echo json_encode($response);
}
?>