<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['dagid']) && isset($data['uid'])) {
    $dagid = $data['dagid'];
    $uid = $data['uid'];
    if ($db->isHasFav($uid, $dagid)) {
        if ($db->setFavDagangan($dagid, $uid)) {
            $response['status'] = "success";
            $response['message'] = "Success Save Dagangan";
            $favDetail = $db->getFavDagangan($uid);
            $response["fav_data"] = $favDetail;
            echo json_encode($response);
        } else {
            $response['status'] = "failed";
            $response['message'] = "Failed Save Dagangan";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Dagangan Has Favourite";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
