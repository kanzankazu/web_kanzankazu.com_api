<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

$data = json_decode(file_get_contents('php://input'), true);
// json response array
$response = array();

if (isset($data["uid"])) {
    $uid = $data['uid'];
    $qnaData = $db->getQNAByUID($uid);
    if ($qnaData) {
        $response["status"] = "success";
        $response["message"] = "Success get data";
        $response["tanya1"] = $qnaData["tanya1"];
        $response["tanya2"] = $qnaData["tanya2"];
        $response["jawab1"] = $qnaData["jawab1"];
        $response["jawab2"] = $qnaData["jawab2"];
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["message"] = "No data";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "invalid parameter";
    echo json_encode($response);
}

?>