<?php

class Other_DB_Functions
{

    private $conn;

    // constructor
    function __construct()
    {
        require_once '1DB_Connect.php';
        // koneksi ke database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    function __destruct()
    {

    }


    public function getOuiData()
    {
        $stmt = $this->conn->prepare("SELECT `mac`,`name` FROM oui");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function isOuiReady($mac)
    {
        $stmt = $this->conn->prepare("SELECT * FROM oui WHERE mac=?");
        $stmt->bind_param("s", $mac);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function setOuiData($mac, $name)
    {
        $stmt = $this->conn->prepare("
                INSERT INTO
                oui(mac, name)
                VALUES (?,?)");
        $stmt->bind_param("ss", $mac, $name);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
}

?>