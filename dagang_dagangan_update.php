<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['uid']) && isset($data['dagid']) && isset($data['dagtype'])) {
    if (!empty($data['daglat']) && !empty($data['daglng'])) {
        $dagid = $data['dagid'];

        $uid = $data['uid'];
        $dagangIdCtgy = $data['dagctgyid'];
        $dagangNm = $data['dagnm'];
        $dagangSlogan = $data['dagslogan'];
        $dagangType = $data['dagtype'];
        $dagangStart = $data['dagstart'];
        $dagangEnd = $data['dagend'];
        $dagangAuto = $data['dagauto'];
        $dagangOrder = $data['dagorder'];
        $dagangOnlyOrder = $data["dagorderonly"];
        $dagangDpOrder = $data["dagorderdp"];
        $dagangDpMinOrder = $data["dagorderdpmin"];
        $dagangAgen = $data['dagagen'];
        $dagangSend = $data['dagsend'];
        $dagangPhnNmbr = $data['dagphnmbr'];
        $dagangPhnNmbrVisible = $data['dagphnmbrhide'];
        $dagangLat = $data['daglat'];
        $dagangLng = $data['daglng'];
        $dagangAddr = $data['dagaddr'];
        $barangNm = $data['dagbrngnm'];


        if ($db->isUidDagExisted($uid)) {
            if ($db->isdagIdExisted($dagid)) {
                $updateDagangan = $db->updateDagangan($dagangNm, $dagangSlogan, $dagangPhnNmbr, $dagangPhnNmbrVisible, $dagangIdCtgy, $dagangType, $dagangStart, $dagangEnd, $dagangAuto, $dagangOrder, $dagangOnlyOrder, $dagangDpOrder, $dagangDpMinOrder, $dagangSend, $dagangAgen, $dagangAddr, $dagangLat, $dagangLng, $dagid, $uid);
                if ($updateDagangan) {
                    $response["status"] = "success";
                    $response["message"] = "Success Update Dagangan";
                    echo json_encode($response);
                } else {
                    $response["status"] = "failed";
                    $response["message"] = "Failed Update Dagangan";
                    echo json_encode($response);
                }
            } else {
                $response["status"] = "failed";
                $response["message"] = "Dagangan not Already";
                echo json_encode($response);
            }
        } else {
            $response["status"] = "failed";
            $response["message"] = "Pedagangan not Already";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Location Dagangan Not Detect";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameter";
    echo json_encode($response);
}

?>