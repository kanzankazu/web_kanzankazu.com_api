<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['dagid'])) {
    $dagId = $data['dagid'];
    if ($db->isdagIdExisted($dagId)) {
        if ($db->deleteDagangan($dagId)) {
            $response["status"] = "success";
            $response["message"] = "Success Delete Dagangan";
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["message"] = "Failed Delete Dagangan";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "dagangan has deleted / dagangan not already";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid parameters";
    echo json_encode($response);
}
?>