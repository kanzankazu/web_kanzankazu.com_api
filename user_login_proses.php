<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data["email"]) && isset($data["password1"])) {

    $email = $data['email'];
    $password1 = $data['password1'];
    $passmd = md5($password1);
    $time = time();

    if ($db->isEmPassExisted($email, $passmd)) {
        if ($db->isMailConfirm($email, 1)) {
            if ($db->isStatusActive($email, 1)) {
                if ($db->updateActiveDt($email)) {
                    $response["status"] = "success";
                    $response["message"] = "Login Success";
                    $users = $db->getUserByEmailPassword($email, $passmd);
                    $response["uid"] = $users["uid"];
                    $response["userlevel"] = $users["userlevel"];
                    $response["email"] = $users["email"];
                    $response["name"] = $users["name"];
                    $response["birthdt"] = $users["birthdt"];
                    $response["gender"] = $users["gender"];
                    $response["phonenmbr"] = $users["phonenmbr"];
                    $response["usercode"] = $users["usercode"];
                    echo json_encode($response);
                } else {
                    $response["status"] = "failed";
                    $response["message"] = "Login Failed";
                    echo json_encode($response);
                }
            } else {
                $response["status"] = "failed";
                $response["message"] = "Status Is Not Active";
                echo json_encode($response);
            }
        } else {
            $response["status"] = "failed";
            $response["message"] = "You Must Confirm Email";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "User Not Exist";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameter";
    echo json_encode($response);
}
?>