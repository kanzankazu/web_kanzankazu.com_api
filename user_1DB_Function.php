<?php

class User_DB_Functions
{
    private $conn;
    private $crpyt;

    // constructor
    function __construct()
    {
        require_once '1DB_Connect.php';
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }

    function __destruct()
    {

    }


    //AUTH
    public function isEmailExisted($email)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    public function isEmPassExisted($email, $password)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ? AND password=?");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    public function isCodePassExisted($code, $password)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE usercode = ? AND password = ? ");
        $stmt->bind_param("ss", $code, $password);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    public function isUidPassExisted($uid, $password)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE uid = ? AND password = ? ");
        $stmt->bind_param("ss", $uid, $password);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    //LOGIN
    public function updateActiveDt($email)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET activedt=now() WHERE email=?");
        $stmt->bind_param("s", $email);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function isMailConfirm($email, $confirm)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ? AND mailconfirm = ?");
        $stmt->bind_param("ss", $email, $confirm);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function isStatusActive($email, $status)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email=? AND mailconfirm=?");
        $stmt->bind_param("ss", $email, $status);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    //REGISTER
    public function setUser($email, $name, $username, $phonenmbr, $enctpassword, $emailcode, $uid, $usercode)
    {
        /*$uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt*/

        $stmt = $this->conn->prepare("INSERT INTO kanzanusers(uid,email,username,name,phonenmbr,password,activatecode,usercode,registerdt,mailconfirm,status) VALUES (?,?,?,?,?,?,?,?,now(),0,0)");
        $stmt->bind_param("ssssssss", $uid, $email, $username, $name, $phonenmbr, $enctpassword, $emailcode, $usercode);
        $result = $stmt->execute();
        $stmt->close();

        // cek jika sudah sukses
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
        } else {
            return false;
        }
    }

    public function isActivationCode($actcode)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE activatecode=?");
        $stmt->bind_param("s", $actcode);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function getEmailbyCode($actcode)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE activatecode=?");
        $stmt->bind_param("s", $actcode);
        $result = $stmt->execute();
        if ($result) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateConfirm($email)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET mailconfirm=1 , activedt=NOW(), activatecode='',status=1 WHERE email=?");
        $stmt->bind_param("s", $email);
        $result = $stmt->execute();
        if ($result) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateUser($uid, $name, $birthdt, $phonenmbr, $gender)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET NAME=?, birthdt=?, phonenmbr=?, gender=? WHERE uid = ?");
        $stmt->bind_param("sssss", $name, $birthdt, $phonenmbr, $gender, $uid);
        $result = $stmt->execute();
        if ($result) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    //FORGETPASS
    public function isPassValid($password1, $password2)
    {
        if ($password1 == $password2) {
            return true;
        } else {
            return false;
        }
    }

    public function updatePassword($email, $enctpass)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET password=?,passworddt=now() WHERE email=?");
        $stmt->bind_param("ss", $enctpass, $email);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return false;
        }
    }

    public function updatePasswordByCode($code, $enctpass)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET password = '$enctpass' , passworddt = now() WHERE usercode = '$code'");
        //$stmt->bind_param("ss", $enctpass, $code);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt2 = $this->conn->prepare("SELECT * FROM kanzanusers WHERE usercode = '$code'");
            //$stmt->bind_param("s", $code);
            $stmt2->execute();
            $user = $stmt2->get_result()->fetch_assoc();
            $stmt2->close();
            return $user;
        } else {
            return false;
        }
    }

    public function updatePasswordByUid($uid, $enctpass)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET password = '$enctpass' , passworddt = now() WHERE uid = '$uid'");
        //$stmt->bind_param("ss", $enctpass, $code);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt2 = $this->conn->prepare("SELECT * FROM kanzanusers WHERE uid = '$uid'");
            //$stmt->bind_param("s", $code);
            $stmt2->execute();
            $user = $stmt2->get_result()->fetch_assoc();
            $stmt2->close();
            return $user;
        } else {
            return false;
        }
    }

    //QNA
    public function isAnswerExsist($email)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email=?");
        $stmt->bind_param("s", $email);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            if (!empty($user["jawab1"]) && !empty($user['jawab2'])) {
                $stmt->close();
                return true;
            } else {
                $stmt->close();
                return false;
            }
        } else {
            $stmt->close();
            return false;
        }
    }

    public function isQnARight($email, $tanya1, $tanya2, $jawab1, $jawab2)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email=? AND tanya1=? AND jawab1=? AND tanya2=? AND jawab2=?");
        $stmt->bind_param("sssss", $email, $tanya1, $jawab1, $tanya2, $jawab2);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function getQNAByUID($uid)
    {
        $stmt = $this->conn->prepare("SELECT email,tanya1,tanya2,jawab1,jawab2 FROM kanzanusers WHERE uid=?");
        $stmt->bind_param("s", $uid);
        $query = $stmt->execute();
        //$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $result = $stmt->get_result()->fetch_assoc();
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function getQNAByEmail($email)
    {
        $stmt = $this->conn->prepare("SELECT email,tanya1,tanya2,jawab1,jawab2 FROM kanzanusers WHERE email=?");
        $stmt->bind_param("s", $email);
        $query = $stmt->execute();
        //$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $result = $stmt->get_result()->fetch_assoc();
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function getQNAByCode($code)
    {
        $stmt = $this->conn->prepare("SELECT email,tanya1,tanya2,jawab1,jawab2 FROM kanzanusers WHERE usercode=?");
        $stmt->bind_param("s", $code);
        $query = $stmt->execute();
        //$result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $result = $stmt->get_result()->fetch_assoc();
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function setQna($email, $tanya1, $tanya2, $jawab1, $jawab2)
    {
        $stmt = $this->conn->prepare("UPDATE kanzanusers SET tanya1=?, tanya2=?, jawab1=?, jawab2=? WHERE email=?");
        $stmt->bind_param("sssss", $tanya1, $tanya2, $jawab1, $jawab2, $email);
        $result = $stmt->execute();
        if ($result) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return true;
        }
    }

    //PROFILE
    public function getUserByEmail($email)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function getUserByCode($code)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE usercode = ?");
        $stmt->bind_param("s", $code);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function getUserByUid($uid)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE uid = ?");
        $stmt->bind_param("s", $uid);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function getUserByEmailPassword($email, $password)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE email = ? AND password = ?");
        $stmt->bind_param("ss", $email, $password);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            // autentikasi user berhasil
            return $user;
        } else {
            $stmt->close();
            return NULL;
        }
    }

    public function getAllUserData()
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function hashSSHA($password)
    {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    public function checkhashSSHA($salt, $password)
    {
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return $hash;
    }

    //REPORT
    public function setReport($name, $email, $website, $phonenmbr, $msg, $mailcode, $ipdevice, $devicedetail, $token, $appPkg, $appVersion)
    {
        $stmt = $this->conn->prepare("INSERT INTO kanzanmail(name, email, phone, website, message, device, crtdt, ipaddress, mailcode, status,token,apppkg,appversion) VALUES (?,?,?,?,?,?,now(),?,?,1,?,?,?)");
        $stmt->bind_param("sssssssssss", $name, $email, $phonenmbr, $website, $msg, $devicedetail, $ipdevice, $mailcode, $token, $appPkg, $appVersion);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function isReportActive($devicedetail, $appPkg)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanmail WHERE device = ? AND apppkg = ? AND status = 1");
        $stmt->bind_param("ss", $devicedetail, $appPkg);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;

        }
    }

    public function send_email($tujuanemail, $namaemail, $subjectemail, $isiemail)
    {
        require('library/PHPMailer-master/class.phpmailer.php');
        require("library/PHPMailer-master/class.smtp.php");
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        //$mail->SMTPDebug = 2;
        $mail->Username = "kanzan46@gmail.com";
        $mail->Password = "kanzan1212";
        $webmaster_email = "kanzan46@gmail.com";
        $mail->From = $webmaster_email;
        $mail->FromName = "kanzankazu";
        $mail->addAddress($tujuanemail, $namaemail);
        $mail->AddReplyTo($webmaster_email, $mail->FromName);
        $mail->WordWrap = 50;
        $mail->IsHTML(true);
        $mail->Subject = "$subjectemail";
        $mail->Body = $isiemail;
        if ($mail->Send()) {
            return true;
        } else {
            return "Gagal mengirim email: " . $mail->ErrorInfo;
        }
    }

    public function isPhoneExisted($phonenmbr)
    {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE phonenmbr = ?");
        $stmt->bind_param("s", $phonenmbr);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }


    public function getAllCategory()
    {
        $stmt = $this->conn->prepare("SELECT DISTINCT dagctgyid,dagctgynm FROM daganganctgy ORDER BY dagctgyid ASC");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function createPass($pass)
    {
        return $this->crpyt->hash_password($pass);
    }
}

?>