<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

$data = json_decode(file_get_contents('php://input'), true);

// json response array
$response = array();

if (isset($data['email'])) {

    $email = $data['email'];
    $user = $db->getUserByEmail($email);

    if ($db->isEmailExisted($email)) {
        if ($db->isAnswerExsist($email)) {

            $response["status"] = "success";
            $response["message"] = "QnA Answered";
            $users = $db->getUserByEmail($email);
            $response["uid"] = $users["uid"];
            $response["userlevel"] = $users["userlevel"];
            $response["email"] = $users["email"];
            $response["name"] = $users["name"];
            $response["birthdt"] = $users["birthdt"];
            $response["gender"] = $users["gender"];
            $response["phonenmbr"] = $users["phonenmbr"];
            $response["usercode"] = $users["usercode"];
            echo json_encode($response);
        } else {

            $response["status"] = "failed";
            $response["message"] = "QnA Not Yet Answered";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Email Unregistered";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}
?>