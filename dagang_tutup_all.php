<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['uid'])) {
    $uid = $data['uid'];
    if ($db->setTutupAll($uid)) {
        $response['status'] = "success";
        $response['message'] = "Close Success";
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response['message'] = "Close Failed";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>