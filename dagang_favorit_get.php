<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

$favDetail = $db->getFavDagangan($data['uid']);
if ($favDetail) {
    $response['status'] = "success";
    $response['message'] = "Success get Favorite";
    $response["data"] = $favDetail;
    echo json_encode($response);
} else {
    $response['status'] = "failed";
    $response['message'] = "Failed get Favorite";
    echo json_encode($response);
}
?>
