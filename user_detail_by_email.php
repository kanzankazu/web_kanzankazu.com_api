<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['email'])) {

    $email = $data['email'];
    $users = $db->getUserByEmail($email);
    if ($users) {
        $response["status"] = "success";
        $response["message"] = "Success Get User";
        $response["data"]["uid"] = $users["uid"];
        $response["data"]["userlevel"] = $users["userlevel"];
        $response["data"]["email"] = $users["email"];
        $response["data"]["name"] = $users["name"];
        $response["data"]["usercode"] = $users["usercode"];
        echo json_encode($response);
    } else {
        $response["status"] = "failed";
        $response["message"] = "Failed to get User";
        echo json_encode($response);
    }
} else {

    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}
?>