<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data["state"]) && isset($data["dagid"])) {
    $state = $data["state"];
    $dagid = $data["dagid"];
    if ($db->isdagIdExisted($dagid)) {
        if ($db->setBukaTutup($state, $dagid)) {
            $response['status'] = "success";
            $response['message'] = $state . " Success";
            echo json_encode($response);
        } else {
            $response['status'] = "failed";
            $response['message'] = $state . " Failed";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "dagangan not already";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>