<?php
require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['uid']) && isset($data['password1']) && isset($data['password2'])) {

    $uid = $data['uid'];
    $pass1 = ($data['password1']);
    $passmd1 = md5($pass1);
    $pass2 = ($data['password2']);
    $passmd2 = md5($pass2);

    if ($db->isPassValid($passmd1, $passmd2)) {
        $users = $db->updatePasswordByUid($uid, $passmd1);
        if ($users) {
            $response["status"] = "success";
            $response["message"] = "Success Update Password";
            $response["uid"] = $users["uid"];
            $response["userlevel"] = $users["userlevel"];
            $response["email"] = $users["email"];
            $response["name"] = $users["name"];
            $response["birthdt"] = $users["birthdt"];
            $response["gender"] = $users["gender"];
            $response["phonenmbr"] = $users["phonenmbr"];
            $response["usercode"] = $users["usercode"];
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["message"] = "Failed Update Password";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Invalid Password";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}

?>