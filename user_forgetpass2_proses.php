<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['email']) && isset($data['tanya1']) && isset($data['jawab1']) && isset($data['tanya2']) && isset($data['jawab2'])) {

    $email = $data['email'];
    $tanya1 = $data['tanya1'];
    $jawab1 = $data['jawab1'];
    $tanya2 = $data['tanya2'];
    $jawab2 = $data['jawab2'];

    if ($db->isEmailExisted($email)) {
        if ($db->isQnARight($email, $tanya1, $jawab1, $tanya2, $jawab2)) {
            $response["status"] = "success";
            $response["message"] = "QnA Valid";
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["message"] = "Invalid QnA";
            echo json_encode($response);
        }
    } else {
        $response["status"] = "failed";
        $response["message"] = "Email Unregistered";
        echo json_encode($response);
    }
} else {
    $response["status"] = "failed";
    $response["message"] = "Invalid Parameters";
    echo json_encode($response);
}
?>