<?php

require_once 'user_1DB_Function.php';
$db = new User_DB_Functions();

//header('Content-Type: application/json');

// json response array
$response = array();

if (isset($_GET["id"])) {
    $confirm = $_GET['id'];
    $isActiveCode = $db->isActivationCode($confirm);
    if ($isActiveCode) {
        $email = $db->getEmailbyCode($confirm)["email"];
        $updateConfirm = $db->updateConfirm($email);
        if ($updateConfirm) {
            //echo "alert('Konfirmasi Berhasil, Silahkan kembali ke Aplikasi untuk Login');";
            //header("Location: http://kanzankazu.com");
            //echo 'window.location.href = "https://www.google.co.id";';
            $msg = "Konfirmasi Berhasil, Silahkan kembali ke Aplikasi untuk Login";
            echo "<script type='text/javascript'>
            alert('$msg');
            window.close();
            </script>";
        } else {
            $msg = "Konfirmasi gagal, silahkan hubungi administrator";
            echo "<script type='text/javascript'>
            alert('$msg');
            window.close();
            </script>";
        }
    } else {
        $msg = "ID tidak dikenali/ sudah aktif";
        echo "<script type='text/javascript'>
            alert('$msg');
            window.close();
            </script>";
    }
} else {
    $msg = "Nothing to do";
    echo "<script type='text/javascript'>
            alert('$msg');
            window.close();
            </script>";
}
?>