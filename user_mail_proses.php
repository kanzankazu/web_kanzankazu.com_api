<?php
require_once 'user_1DB_Function.php';
require_once '12_Config_support.php';
$db = new User_DB_Functions();
header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (
    isset($data['name']) &&
    isset($data['email']) &&
    isset($data['msg']) &&
    isset($data['ip']) &&
    isset($data['detaildevice'])
) {

    //$mailid = get_rownum_table('kanzanmail', 'mailid');
    $mailcode = gen_uuid3();
    $name = $data['name'];
    $email = $data['email'];
    $phonenmbr = $data['phonenmbr'];
    $website = $data['website'];
    $msg = $data['msg'];
    $ipdevice = $data['ip'];
    $devicedetail = $data['detaildevice'];
    $token = $data['token'];
    $appPkg = $data['apppkg'];
    $appVersion = $data['appversion'];

    $isReportActived = $db->isReportActive($devicedetail, $appPkg, $appVersion);
    if (!$isReportActived) {
        $setReport = $db->setReport($name, $email, $website, $phonenmbr, $msg, $mailcode, $ipdevice, $devicedetail, $token, $appPkg, $appVersion);
        if ($setReport) {
            $sendEmail = $db->send_email($email, $name, 'KANZANKAZU.COM (ticket message #' . $mailcode . ')', 'Pesan: ' . $msg);
            if ($sendEmail) {
                $response['status'] = "success";
                $response['message'] = "Report Send";
                echo json_encode($response);
            } else {
                $response['status'] = "failed";
                $response['message'] = $sendEmail;
                echo json_encode($response);
            }
        } else {
            $response['status'] = "failed";
            $response['message'] = "Report Not Saved";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Previous Reports Are Being Processed/ Actived";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameter";
    echo json_encode($response);
}
?>
