<?php

class Dagangan_DB_Function {
    private $conn;

    // constructor
    function __construct() {
        require_once '1DB_Connect.php';
        require_once '12_Config_support.php';
        // koneksi ke database
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }

    function __destruct() {

    }

    //
    public function getLastNumTable($TableName, $RowName) {
        $stmt = $this->conn->prepare("SELECT $RowName FROM $TableName ORDER BY $RowName DESC LIMIT 1");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_assoc()[$RowName];
        if ($query) {
            return $result + 1;
        } else {
            return false;
        }
    }

    //check
    public function isUserExist($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE uid = ?");
        $stmt->bind_param("s", $uid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows >= 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function isUidDagExisted($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM dagangan WHERE uid = ?");
        $stmt->bind_param("s", $uid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    public function isdagIdExisted($dagId) {
        $stmt = $this->conn->prepare("SELECT * FROM dagangan WHERE dagid = ?");
        $stmt->bind_param("s", $dagId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            // user ada
            $stmt->close();
            return true;
        } else {
            // user tidak ada
            $stmt->close();
            return false;
        }
    }

    public function isDaganganReady($dagangid, $uid) {
        $stmt = $this->conn->prepare("SELECT * FROM dagangan WHERE dagid=? AND uid=?");
        $stmt->bind_param("ss", $dagangid, $uid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function isBarangReady($dagangid, $barangNm) {
        $stmt = $this->conn->prepare("SELECT * FROM daganganbrg WHERE dagid=? AND dagbrngnm=?");
        $stmt->bind_param("ss", $dagangid, $barangNm);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function getCountDagangan($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM dagangan WHERE uid = ? ");
        $stmt->bind_param("s", $uid);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }

    public function getLimitDagangan($uid) {
        $stmt = $this->conn->prepare("SELECT * FROM kanzanusers WHERE uid = ? ");
        $stmt->bind_param("s", $uid);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_assoc()['daglimit'];
        $stmt->close();
        return $result;
    }

    public function getAllCategory() {
        $stmt = $this->conn->prepare("SELECT DISTINCT dagctgyid,dagctgynm FROM daganganctgy ORDER BY dagctgyid ASC");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function getAllCategoryTest() {
        $stmt = $this->conn->prepare("SELECT DISTINCT dagctgyid,dagctgynm FROM daganganctgy ORDER BY dagctgyid ASC LIMIT 2");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function getAllBarangNm() {
        $stmt = $this->conn->prepare("SELECT DISTINCT dagbrngnm FROM daganganbrg ORDER BY dagbrngnm ASC");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    //dagangan
    public function checkDagangan($dagangid, $uid) {
        $stmt = $this->conn->prepare("SELECT * FROM dagangan WHERE dagid=? AND uid=? ");
        $stmt->bind_param("ss", $dagangid, $uid);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function setDagangan($dagangId, $uid, $dagangIdCtgy, $dagangNm, $dagangSlogan, $dagangType, $dagangOrder, $dagangOnlyOrder, $dagangDpOrder, $dagangDpMinOrder, $dagangSend, $dagangAgen, $dagangPhnNmbrVisible, $dagangPhnNmbr, $dagangLat, $dagangLng, $dagangAddr, $dagangAuto, $dagangStart, $dagangEnd, $dagangCode) {
        $stmt = $this->conn->prepare("
                INSERT INTO
                dagangan(dagid, uid, dagctgyid, dagnm, dagslogan, dagtype, dagorder, dagorderonly, dagorderdp, dagorderdpmin, dagsend, dagagen, dagphnmbrhide, dagphnmbr, daglat, daglng, dagaddr, dagauto, dagstart, dagend, dagactvdt, dagcrtdt, dagexprdt, dagcode)
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now(),date_add(now(),INTERVAL 3 MONTH),?)");
        $stmt->bind_param("sssssssssssssssssssss", $dagangId, $uid, $dagangIdCtgy, $dagangNm, $dagangSlogan, $dagangType, $dagangOrder, $dagangOnlyOrder, $dagangDpOrder, $dagangDpMinOrder, $dagangSend, $dagangAgen, $dagangPhnNmbrVisible, $dagangPhnNmbr, $dagangLat, $dagangLng, $dagangAddr, $dagangAuto, $dagangStart, $dagangEnd, $dagangCode);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateDagangan($dagangNm, $dagangSlogan, $dagangPhnNmbr, $dagangPhnNmbrVisible, $dagangIdCtgy, $dagangType, $dagangStart, $dagangEnd, $dagangAuto, $dagangOrder, $dagangOnlyOrder, $dagangDpOrder, $dagangDpMinOrder, $dagangSend, $dagangAgen, $dagangAddr, $dagangLat, $dagangLng, $dagangId, $uid) {
        $stmt = $this->conn->prepare("
          UPDATE dagangan
          SET
          dagnm=?,
          dagslogan=?,
          dagphnmbr=?,
          dagphnmbrhide=?,
          dagctgyid=?,
          dagtype=?,
          dagstart=?,
          dagend=?,
          dagauto=?,
          dagorder=?,
          dagorderonly=?,
          dagorderdp=?,
          dagorderdpmin=?,
          dagsend=?,
          dagagen=?,
          dagaddr=?,
          daglat=?,
          daglng=?
          WHERE dagid=? AND uid=?");
        $stmt->bind_param("ssssssssssssssssssss",
            $dagangNm,
            $dagangSlogan,
            $dagangPhnNmbr,
            $dagangPhnNmbrVisible,
            $dagangIdCtgy,
            $dagangType,
            $dagangStart,
            $dagangEnd,
            $dagangAuto,
            $dagangOrder,
            $dagangOnlyOrder,
            $dagangDpOrder,
            $dagangDpMinOrder,
            $dagangSend,
            $dagangAgen,
            $dagangAddr,
            $dagangLat,
            $dagangLng,
            $dagangId,
            $uid
        );
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteDagangan($dagangid) {
        $stmt = $this->conn->prepare("DELETE FROM dagangan WHERE dagid = ?");
        $stmt->bind_param("i", $dagangid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            $stmt1 = $this->conn->prepare("UPDATE daganganbrg SET dagid = 0 WHERE dagid = ?");
            $stmt1->bind_param("i", $dagangid);
            $result1 = $stmt1->execute();
            $stmt1->close();
            if ($result1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //barang
    public function checkBarang($dagangid, $barangNm) {
        $stmt = $this->conn->prepare("SELECT * FROM daganganbrg WHERE dagid=? AND dagbrngnm=?");
        $stmt->bind_param("ss", $dagangid, $barangNm);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function setBarangArray($barangid, $dagangid, $barangnm) {
        $barangNmListNum = substr_count($barangnm, ",") + 1;
        if ($barangNmListNum > 1) {
            $barangNmList = explode(',', $barangnm);
            foreach ($barangNmList as $barangValue) {
                //$addbarang = mysql_query("INSERT INTO daganganbrg(dagid, dagbrngnm) VALUES ('$barangid','$barangValue')")
                //OR die("Query failed with error: " . mysql_error());
                $stmt = $this->conn->prepare("INSERT INTO daganganbrg (dagid, dagbrngnm) VALUES (?,?)");
                $stmt->bind_param("ss", $dagangid, $barangnm);
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    return true;
                } else {
                    return false;
                }
            }
        } elseif ($barangNmListNum == 1) {
            $stmt = $this->conn->prepare("INSERT INTO daganganbrg (dagbrngid, dagid, dagbrngnm) VALUES (?,?,?)");
            $stmt->bind_param("sss", $barangid, $dagangid, $barangnm);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function setBarang($dagangid, $barangnm) {
        $stmt = $this->conn->prepare("INSERT INTO daganganbrg ( dagid, dagbrngnm) VALUES (?,?)");
        $stmt->bind_param("ss", $dagangid, $barangnm);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateBarang($dagbrngnm, $dagbrngid) {
        $stmt = $this->conn->prepare("UPDATE daganganbrg SET dagbrngnm=? WHERE dagbrngid=? ");
        $stmt->bind_param("ss", $dagbrngnm, $dagbrngid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBarang($barangNm, $dagangid) {
        $stmt = $this->conn->prepare("DELETE FROM daganganbrg WHERE dagbrngnm = ? AND dagid = ? ");
        $stmt->bind_param("ss", $barangNm, $dagangid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    //buka tutup
    public function setBukaAll($uid) {
        $stmt = $this->conn->prepare("UPDATE dagangan SET dagstat = 'buka' WHERE uid = ?");
        $stmt->bind_param("s", $uid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function setTutupAll($uid) {
        $stmt = $this->conn->prepare("UPDATE dagangan SET dagstat = 'tutup' WHERE uid = ?");
        $stmt->bind_param("s", $uid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function setBukaTutup($bukatutup, $dagangid) {
        if (isset($bukatutup)) {
            if ($bukatutup == "buka") {
                $stmt = $this->conn->prepare("UPDATE dagangan SET dagstat = 'tutup' WHERE dagid = ?");
            } else if ($bukatutup == "tutup") {
                $stmt = $this->conn->prepare("UPDATE dagangan SET dagstat = 'buka' WHERE dagid = ?");
            } else {
                $stmt = $this->conn->prepare("UPDATE dagangan SET dagstat = 'tutup' WHERE dagid = ?");
            }
            $stmt->bind_param("s", $dagangid);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //fav
    public function isHasFav($uidFav, $dagangidfav) {
        $stmt = $this->conn->prepare("SELECT * FROM daganganfav WHERE uid=? AND dagid=?");
        $stmt->bind_param("ss", $uidFav, $dagangidfav);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function checkDagFavId($dagangidfav) {
        $stmt = $this->conn->prepare("SELECT * FROM daganganfav WHERE dagid=?");
        $stmt->bind_param("s", $dagangidfav);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function setFavDagangan($dagangidfav, $uidFav) {
        $stmt = $this->conn->prepare("INSERT INTO daganganfav(dagid, uid) VALUES(?,?)");
        $stmt->bind_param("ss", $dagangidfav, $uidFav);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteFavDagangan($favid) {
        $stmt = $this->conn->prepare("DELETE FROM daganganfav WHERE dagfavid = ?");
        $stmt->bind_param("s", $favid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getFavDagangan($uidFav) {
        $stmt = $this->conn->prepare("
            SELECT
              *
            FROM dagangan AS dag
              JOIN daganganfav AS fav
                ON fav.dagid=dag.dagid
              JOIN daganganbrg AS bd
                ON bd.dagid = dag.dagid
              JOIN daganganctgy AS ctgy
                ON ctgy.dagctgyid = dag.dagctgyid
            WHERE fav.uid=?
            GROUP BY fav.uid,dag.dagnm");
        $stmt->bind_param("s", $uidFav);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
            var_dump($result);
        } else {
            return false;
        }
    }

    //review
    public function checkRev($uid, $dagId) {
        $stmt = $this->conn->prepare("SELECT * FROM daganganreview WHERE uid=? AND dagid=?");
        $stmt->bind_param("ss", $uid, $dagId);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function setRevDagangan($dagRevId, $dagRevNm, $dagid, $dagrevrating, $uid) {
        $stmt = $this->conn->prepare("INSERT INTO daganganreview(dagrevid, dagrevnm, dagid, dagrevrating,  uid) VALUES (?,?,?,?,?) ");
        $stmt->bind_param("ssssss", $dagRevId, $dagRevNm, $dagid, $dagrevrating, $uid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateRevDagangan($dagrevnm, $dagrevrating, $dagrevid) {
        $stmt = $this->conn->prepare("UPDATE daganganreview SET dagrevnm=?,dagrevrating=? WHERE dagrevid=?");
        $stmt->bind_param("sss", $dagrevnm, $dagrevrating, $dagrevid);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteRevDagangan() {
        $stmt = $this->conn->prepare("DELETE FROM daganganreview WHERE dagrevid=?");
        $stmt->bind_param("s", $dagRevId);
        $result = $stmt->execute();
        $stmt->close();
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getRevDaganganAll() {
        $stmt = $this->conn->prepare("SELECT * FROM daganganreview WHERE dagid=?");
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    //search
    public function searchByDagId($dagid) {
        $stmt = $this->conn->prepare("
      SELECT
      * ,
      group_concat(bd.dagbrngnm ORDER BY bd.dagbrngnm SEPARATOR',') AS barangnmlist
      FROM dagangan AS d
        /*JOIN kanzanusers AS us
        ON us.uid = d.uid*/
        JOIN daganganbrg AS bd
        ON bd.dagid = d.dagid
        JOIN daganganctgy AS ctgy
        ON ctgy.dagctgyid = d.dagctgyid
      WHERE d.dagid = ?
      GROUP BY /*us.uid,*/d.dagnm");
        $stmt->bind_param("s", $dagid);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_assoc();
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchNmBarang($lat, $lng, $cari, $distance) {
        $stmt = $this->conn->prepare("SELECT
        *,
        GROUP_CONCAT(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist,
        (6371 * ACOS(	COS(RADIANS($lat))
         * 	COS(RADIANS(d.daglat))
         * 	COS(RADIANS(d.daglng) - RADIANS($lng))
         + 	SIN(RADIANS($lat))
            * 	SIN(RADIANS(d.daglat))
          )
          ) AS location
        FROM dagangan AS d
          /*JOIN kanzanusers AS us
            ON us.uid = d.uid*/
          JOIN daganganbrg AS bd
            ON bd.dagid = d.dagid
          JOIN daganganctgy AS ctgy
            ON ctgy.dagctgyid = d.dagctgyid
        WHERE bd.dagbrngnm LIKE '%$cari%' /*AND d.dagstat = 'buka'*/
        GROUP BY /*us.uid,*/d.dagnm
        HAVING location < (($distance))");
        //$stmt->bind_param("s", $cari);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchDagangan($lat, $lng, $cari, $distance) {
        $stmt = $this->conn->prepare("SELECT
        *,
        GROUP_CONCAT(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist,
        (6371 * ACOS(	COS(RADIANS($lat))
         * 	COS(RADIANS(d.daglat))
         * 	COS(RADIANS(d.daglng) - RADIANS($lng))
         + 	SIN(RADIANS($lat))
            * 	SIN(RADIANS(d.daglat))
          )
          ) AS location
        FROM dagangan AS d
          /*JOIN kanzanusers AS us
            ON us.uid = d.uid*/
          JOIN daganganbrg AS bd
            ON bd.dagid = d.dagid
          JOIN daganganctgy AS ctgy
            ON ctgy.dagctgyid = d.dagctgyid
        WHERE d.dagnm LIKE '%$cari%' /*AND d.dagstat = 'buka'*/
        GROUP BY /*us.uid,*/d.dagnm
        HAVING location < (($distance))");
        //$stmt->bind_param("sssss", $lat, $lng, $lat, "%" . $cari . "%", $distance);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchPemilik($lat, $lng, $cari, $distance) {
        $stmt = $this->conn->prepare("SELECT
        *,
        GROUP_CONCAT(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist,
        (6371 * ACOS(	COS(RADIANS($lat))
         * 	COS(RADIANS(d.daglat))
         * 	COS(RADIANS(d.daglng) - RADIANS($lng))
         + 	SIN(RADIANS($lat))
            * 	SIN(RADIANS(d.daglat))
          )
          ) AS location
        FROM dagangan AS d
          JOIN kanzanusers AS us
            ON us.uid = d.uid
          JOIN daganganbrg AS bd
            ON bd.dagid = d.dagid
          JOIN daganganctgy AS ctgy
            ON ctgy.dagctgyid = d.dagctgyid
        WHERE us.name LIKE '$cari%' /*AND d.dagstat = 'buka'*/
        GROUP BY /*us.uid,*/d.dagnm
        HAVING location < (($distance))");
        //$stmt->bind_param("sssss", $lat, $lng, $lat, $cari . "%", $distance);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchEmail($cariemail) {
        $stmt = $this->conn->prepare("SELECT
      *,
      group_concat(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist
    FROM dagangan AS d
      JOIN kanzanusers AS us
        ON us.uid = d.uid
      JOIN daganganbrg AS bd
        ON bd.dagid = d.dagid
        JOIN daganganctgy AS ctgy
        ON ctgy.dagctgyid = d.dagctgyid
    WHERE us.email LIKE '%$cariemail%'
    GROUP BY us.uid,d.dagnm");
        $stmt->bind_param("s", $cariemail);
        $query = $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchCode($code) {
        $param = "%{$code}%";
        $stmt = $this->conn->prepare("SELECT
      *,
      group_concat(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist
    FROM dagangan AS d
      JOIN kanzanusers AS us
        ON us.uid = d.uid
      JOIN daganganbrg AS bd
        ON bd.dagid = d.dagid
        JOIN daganganctgy AS ctgy
        ON ctgy.dagctgyid = d.dagctgyid
    WHERE us.usercode LIKE ?
    GROUP BY us.uid,d.dagnm");
        $stmt->bind_param("s", $param);
        $query = $stmt->execute() or die('Error' . $stmt->error);
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }

    public function searchUid($uid) {
        $param = "{$uid}%";
        $stmt = $this->conn->prepare("SELECT
      *,
      group_concat(bd.dagbrngnm ORDER BY bd.dagbrngnm) AS barangnmlist
    FROM dagangan AS d
      JOIN kanzanusers AS us
        ON us.uid = d.uid
      JOIN daganganbrg AS bd
        ON bd.dagid = d.dagid
        JOIN daganganctgy AS ctgy
        ON ctgy.dagctgyid = d.dagctgyid
    WHERE us.uid LIKE ?
    GROUP BY us.uid,d.dagnm");
        $stmt->bind_param("s", $param);
        $query = $stmt->execute() or die('Error' . $stmt->error);
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if ($query) {
            return $result;
        } else {
            return false;
        }
    }
}

?>