<?php

require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['dagid']) && isset($data['dagbrngnm'])) {

    // menerima parameter POST
    $dagId = $data['dagid'];
    $dagBrngNm = $data['dagbrngnm'];

    if ($db->isdagIdExisted($dagId)) {
        if ($db->checkBarang($dagId, $dagBrngNm)) {
            if ($db->deleteBarang($dagBrngNm, $dagId)) {
                // app ditemukan update
                $response['status'] = "success";
                $response['message'] = "delete Success";
                echo json_encode($response);
            } else {
                // app tidak ditemukan update
                $response['status'] = "failed";
                $response['message'] = "delete Failed";
                echo json_encode($response);
            }
        } else {
            $response['status'] = "failed";
            $response['message'] = "Barang Not Already";
            echo json_encode($response);
        }
    } else {
        $response['status'] = "failed";
        $response['message'] = "Dagangan Not Already";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}

?>