<?php
require_once 'App_1DB_function.php';
$db = new App_DB_function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['apppkg']) && isset($data['appname']) && isset($data['appversion'])) {

    // menerima parameter POST
    $appPkg = $data['apppkg'];
    $appName = $data['appname'];
    $appVersion = $data['appversion'];

    $app = $db->getAppData($appPkg);
    if ($app) {
        // app ditemukan update
        $response['status'] = "success";
        $response['message'] = "Data Ready";
        $response['data_app']['applink'] = $app['applink'];
        echo json_encode($response);
    } else {
        // app tidak ditemukan update
        $response['status'] = "failed";
        $response['message'] = "Data Empty";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>