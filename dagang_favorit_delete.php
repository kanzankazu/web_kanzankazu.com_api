<?php
require_once 'dagang_1DB_function.php';
require_once '12_Config_support.php';
$db = new Dagangan_DB_Function();

header('Content-Type: application/json');

// json response array
$response = array();

$data = json_decode(file_get_contents('php://input'), true);

if (isset($data['dagfavid']) && isset($data['uid'])) {
    $dagfavid = $data['dagfavid'];
    $uid = $data['uid'];
    if ($db->checkDagFavId($dagfavid)) {
        if ($db->deleteFavDagangan($dagfavid)) {
            $response['status'] = "success";
            $response['message'] = "Delete Fav Success";
            $favDetail = $db->getFavDagangan($uid);
            $response["fav_data"] = $favDetail;
            echo json_encode($response);
        } else {
        }
    } else {
        $response["status"] = "failed";
        $response['message'] = "Fav not already";
        echo json_encode($response);
    }
} else {
    $response['status'] = "failed";
    $response['message'] = "Invalid Parameters";
    echo json_encode($response);
}
?>